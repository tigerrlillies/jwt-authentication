package main

import (
	"time"

	"gitlab.com/tigerrlillies/jwt-authentication/api"
	"gitlab.com/tigerrlillies/jwt-authentication/auth"
	"gitlab.com/tigerrlillies/jwt-authentication/storage"
)

func main() {
	authOptions := &auth.Options{
		AccessToken: &auth.Token{
			SecretKey: []byte("access"),
			ExpireIn:  time.Minute * 3,
		},
		RefreshToken: &auth.Token{
			SecretKey: []byte("refresh"),
			ExpireIn:  time.Minute * 10,
		},
	}

	mockStorage := storage.NewMockStorage()

	API := api.Init(":9010", authOptions, mockStorage)
	API.ListenAndServe()
}
