package api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"

	"gitlab.com/tigerrlillies/jwt-authentication/storage"
)

var (
	ErrSuchUserDoesNotExist = errors.New("such user does not exist")
	ErrInvalidCredentials   = errors.New("credentials are invalid. check login & password")
	ErrTokenIsMissing       = errors.New("token is missing")
	ErrTokenIsExpired       = errors.New("Token is expired")
	ErrInvalidToken         = errors.New("token is invalid")
	ErrInvalidAuthType      = errors.New("authorization must be token-based (Bearer)")
	ErrInvalidAuthHeader    = errors.New("authorization header value has invalid format")
)

type AuthorizationRequest struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type AuthorizationResponse struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

func (api *API) HandleSignUp(w http.ResponseWriter, r *http.Request) {
	buff, err := ioutil.ReadAll(r.Body)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, "unable to read request body", err)
		return
	}

	var req AuthorizationRequest
	if err := json.Unmarshal(buff, &req); err != nil {
		respondWithError(w, http.StatusBadRequest, "failed to decode request body", err)
		return
	}

	if err := api.storage.Write(req.Login, []byte(req.Password)); err != nil {
		respondWithError(w, http.StatusInternalServerError, "unable save credentials to the storage", err)
		return
	}

	accessToken, err := createToken(api.authOptions.AccessToken.ExpireIn, api.authOptions.AccessToken.SecretKey)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, "unable to create token", err)
		return
	}

	refreshToken, err := createToken(api.authOptions.RefreshToken.ExpireIn, api.authOptions.RefreshToken.SecretKey)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, "unable to create token", err)
		return
	}

	respond(w, AuthorizationResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	})
}

func (api *API) HandleResource(w http.ResponseWriter, r *http.Request) {
	authString := r.Header.Get("Authorization")
	if authString == "" {
		respondWithError(w, http.StatusUnauthorized, "unable to authorize", ErrTokenIsMissing)
		return
	}

	splittedAuthString := strings.Split(authString, " ")
	authType, tokenString := splittedAuthString[0], splittedAuthString[1]

	if len(splittedAuthString) != 2 {
		respondWithError(w, http.StatusUnauthorized, "unable to authorize", ErrInvalidAuthHeader)
		return
	}

	if authType != "Bearer" {
		respondWithError(w, http.StatusUnauthorized, "unable to authorize", ErrInvalidAuthType)
		return
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return api.authOptions.AccessToken.SecretKey, nil
	})
	if err == ErrTokenIsExpired {
		respondWithError(w, http.StatusInternalServerError, "unable to authorize", err)
		return
	} else if err != nil {
		respondWithError(w, http.StatusInternalServerError, "error parsing jwt token", err)
		return
	}

	if !token.Valid {
		respondWithError(w, http.StatusUnauthorized, "access not granted", ErrInvalidToken)
	} else {
		respond(w, "here ya go, access granted")
	}
}

func (api *API) HandleLogin(w http.ResponseWriter, r *http.Request) {
	buff, err := ioutil.ReadAll(r.Body)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, "unable to read request body", err)
		return
	}

	var req AuthorizationRequest
	if err := json.Unmarshal(buff, &req); err != nil {
		respondWithError(w, http.StatusBadRequest, "failed to decode request body", err)
		return
	}

	password, err := api.storage.Read(req.Login)
	if err == storage.ErrStorageEntryNotFound {
		respondWithError(w, http.StatusNotFound, "unable to log in", ErrSuchUserDoesNotExist)
		return
	} else if err != nil {
		respondWithError(w, http.StatusInternalServerError, "failed to load data from storage", err)
		return
	}

	if string(password) != req.Password {
		respondWithError(w, http.StatusUnauthorized, "unable to log in", ErrInvalidCredentials)
		return
	}

	accessToken, err := createToken(api.authOptions.AccessToken.ExpireIn, api.authOptions.AccessToken.SecretKey)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, "unable to create token", err)
		return
	}

	refreshToken, err := createToken(api.authOptions.RefreshToken.ExpireIn, api.authOptions.RefreshToken.SecretKey)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, "unable to create token", err)
		return
	}

	respond(w, AuthorizationResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	})
}

func (api *API) HandleRefresh(w http.ResponseWriter, r *http.Request) {
	authString := r.Header.Get("Authorization")
	if authString == "" {
		respondWithError(w, http.StatusUnauthorized, "unable to authorize", ErrTokenIsMissing)
		return
	}

	splittedAuthString := strings.Split(authString, " ")
	authType, tokenString := splittedAuthString[0], splittedAuthString[1]

	if len(splittedAuthString) != 2 {
		respondWithError(w, http.StatusUnauthorized, "unable to authorize", ErrInvalidAuthHeader)
		return
	}

	if authType != "Bearer" {
		respondWithError(w, http.StatusUnauthorized, "unable to authorize", ErrInvalidAuthType)
		return
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		return api.authOptions.RefreshToken.SecretKey, nil
	})
	if err == ErrTokenIsExpired {
		respondWithError(w, http.StatusInternalServerError, "unable to authorize", err)
		return
	} else if err != nil {
		respondWithError(w, http.StatusInternalServerError, "error parsing jwt token", err)
		return
	}

	if !token.Valid {
		respondWithError(w, http.StatusUnauthorized, "unable to refresh tokens", ErrInvalidToken)
	}

	accessToken, err := createToken(api.authOptions.AccessToken.ExpireIn, api.authOptions.AccessToken.SecretKey)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, "unable to create token", err)
		return
	}

	refreshToken, err := createToken(api.authOptions.RefreshToken.ExpireIn, api.authOptions.RefreshToken.SecretKey)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, "unable to create token", err)
		return
	}

	respond(w, AuthorizationResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	})
}

var NotImplemented = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("not implemented"))
	if err != nil {
		log.Println("error writing response: ", err.Error())
	}
})
