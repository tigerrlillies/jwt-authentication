package api

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
)

func respond(w http.ResponseWriter, payload interface{}) {
	buff, err := json.Marshal(payload)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, "failed to json encode response data", err)
	}

	w.Write(buff)
}

func respondWithError(w http.ResponseWriter, statusCode int, message string, err error) {
	wrapped := errors.Wrap(err, message)
	log.Println(wrapped.Error())

	w.WriteHeader(statusCode)
	w.Write([]byte(wrapped.Error()))
}

func createToken(expirationTime time.Duration, secretKey []byte) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"exp": time.Now().Add(expirationTime).Unix(),
	})

	signed, err := token.SignedString(secretKey)
	if err != nil {
		return "", err
	}

	return signed, nil
}
