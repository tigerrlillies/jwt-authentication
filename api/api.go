package api

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/tigerrlillies/jwt-authentication/auth"
	"gitlab.com/tigerrlillies/jwt-authentication/storage"
)

type API struct {
	addr        string
	storage     storage.Storage
	router      *mux.Router
	authOptions *auth.Options
}

func Init(addr string, authOptions *auth.Options, storage storage.Storage) *API {
	router := mux.NewRouter()

	api := &API{
		addr:        addr,
		authOptions: authOptions,
		storage:     storage,
		router:      router,
	}

	router.Handle("/register", http.HandlerFunc(api.HandleSignUp)).Methods(http.MethodPost)
	router.Handle("/login", http.HandlerFunc(api.HandleLogin)).Methods(http.MethodPost)
	router.Handle("/resource", http.HandlerFunc(api.HandleResource)).Methods(http.MethodGet)
	router.Handle("/refresh", http.HandlerFunc(api.HandleRefresh)).Methods(http.MethodGet)

	return api
}

func (api *API) ListenAndServe() {
	err := http.ListenAndServe(api.addr, api.router)
	if err != nil {
		log.Println("http server stopped due to: ", err.Error())
	}
}
