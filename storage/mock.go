package storage

import "errors"

var ErrStorageEntryNotFound = errors.New("no entries found for specified key")

type MockStorage struct {
	data map[string][]byte
}

func NewMockStorage() *MockStorage {
	return &MockStorage{data: make(map[string][]byte)}
}

func (m *MockStorage) Read(key string) ([]byte, error) {
	if val, found := m.data[key]; found {
		return val, nil
	} else {
		return nil, ErrStorageEntryNotFound
	}
}

func (m *MockStorage) Write(key string, payload []byte) error {
	m.data[key] = payload
	return nil
}
