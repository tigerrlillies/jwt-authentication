package storage

type Storage interface {
	Read(key string) ([]byte, error)
	Write(key string, payload []byte) error
}
