package auth

import "time"

type Options struct {
	AccessToken  *Token
	RefreshToken *Token
}

type Token struct {
	SecretKey []byte
	ExpireIn  time.Duration
}
